module.exports = function () {
    var mongoClient = require('mongodb');
    var config = require('../config');
    var assert = require('assert');
    // Connection URL
    var url = `${config.mongoUrl}`;

    var insertDocuments = function (db, callback, documents) {
        // Get the documents collection
        var collection = db.collection('heroes');
        // Insert some documents
        collection.insertMany(documents, function (err, result) {
            console.log(`Inserted ${documents.length} heroes into the collection`);
            callback(result);
        });
    };

    return {
        getDetails: function (id, callback) {
            // Use connect method to connect to the server
            mongoClient.connect(url, function (err, db) {
                assert.equal(err, null);
                var collection = db.collection('heroes');
                collection.find({ "_id": mongoClient.ObjectID(id)}).toArray().then(function (items) {
                    db.close();
                    callback({id: items[0]._id, name: items[0].name})
                })
            });
        },
        delete: function (id, callback) {
            // Use connect method to connect to the server
            mongoClient.connect(url, function (err, db) {
                assert.equal(err, null);
                var collection = db.collection('heroes');
                collection.deleteOne({ "_id": mongoClient.ObjectID(id)}).then(function (items) {
                    db.close();
                    callback(true);
                })
            });
        },
        getAll: function (callback) {
            mongoClient.connect(url, function (err, db) {
                assert.equal(err, null);
                var collection = db.collection('heroes');
                collection.find({}).sort({"_id": 1}).toArray().then(function (items) {
                    db.close();
                    callback(items.map(function (item) {
                        return { id: item._id, name: item.name };
                    }));
                });
            });
        },
        searchByName: function (text, callback) {
            mongoClient.connect(url, function (err, db) {
                assert.equal(err, null);
                var collection = db.collection('heroes');
                collection.find({"name": {$regex: text}}).sort({"_id": 1}).toArray().then(function (items) {
                    db.close();
                    callback(items.map(function (item) {
                        return { id: item._id, name: item.name };
                    }));
                });
            });
        },
        addOrUpdate: function (hero, callback) {
            assert.notEqual(hero, null);
            mongoClient.connect(url, function (err, db) {
                assert.equal(err, null);
                var collection = db.collection('heroes');
                if (!hero.id) {
                    hero.id = new mongoClient.ObjectID();
                }
                else{
                    hero.id = mongoClient.ObjectID(hero.id);
                }
                collection.update(
                    { "_id": hero.id },
                    {
                        _id: hero.id,
                        name: hero.name
                    },
                    { upsert: true }).then(function () {
                        db.close();

                        callback(hero)
                    });

            });
        }
    }
}