var express = require('express');
var router = express.Router();
var heroesController = require('../controllers/heroes')();
var accessControlHeader = "Access-Control-Allow-Origin";

router.options('/*', function (req, res, next) {
    console.log('!OPTIONS');
    var headers = {};
    // IE8 does not allow domains to be specified, just the *
    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
    headers[accessControlHeader] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
    res.writeHead(200, headers);
    res.end();
});
/* GET home page. */
router.get('/heroes', function (req, res, next) {
    heroesController.heroes(req,res);
});

router.get('/heroes/:id', function (req, res, next) {
    heroesController.details(req,res);
});
router.get('/search/:id', function (req, res, next) {
    heroesController.search(req,res);
});

router.delete('/heroes/:id', function (req, res, next) {
    heroesController.delete(req,res);
});

router.post('/heroes', function (req, res, next) {
    heroesController.updateOrCreate(req,res);
});

router.put('/heroes/:id', function (req, res, next) {
    heroesController.updateOrCreate(req,res);
});

module.exports = router;