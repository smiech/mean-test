var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
 
var heroesApi = require('./routes/heroes-api');
 
var app = express();
var port = process.env.PORT || 3000;   


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
// Point static path to dist
app.use(express.static(path.join(__dirname, 'client/dist')));
 
app.use('/api/', heroesApi);

// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });

// Catch all other routes and return the index file
app.use('*',(req, res) => {
  res.sendFile(path.join(__dirname, 'client/dist/index.html'));
});

var host = 'localhost';
var server = app.listen(port, host, function() {
    var port = server.address().port;
    console.log('App listening at http://%s:%s', host, port);
});
 
module.exports = app;