module.exports = function () {
    var heroesRepository = require('../repositories/heroes.repository')();

    return {
        dashboard: function (req, res) {
            heroesRepository.getTop(4, function (data) {
                res.json(data);
            });
        },
        heroes: function (req, res) {
            heroesRepository.getAll(function (data) {
                res.json(data);
            });
        },
        details: function (req, res) {
            heroesRepository.getDetails(req.params.id, function (data) {
                res.json(data);
            });
        },
        delete: function (req, res) {
            heroesRepository.delete(req.params.id, function (data) {
                res.json(true);
            });
        },
        updateOrCreate: function (req, res) {
            var hero = { id: req.body.id, name: req.body.name };
                heroesRepository.addOrUpdate(hero, function (data) {
                    res.json(data);
                });
        },
        search: function (req, res) {
            heroesRepository.searchByName(req.params.id, function (data) {
                res.json(data);
            });
        }
    }
}